package com.somospnt.log;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import static org.junit.Assert.*;
import org.junit.Test;

public class LogGeneratorTest {

    private final String[] LOG_LINES = {
        "<Error> <APM> <BEA-000000> <|E |10:12:21 |52 |server |CorrelationID=OmsServer_52_1472821941500 |StepID=YfsAAA8AyVcKpzSzj6g |OA=28330 |Activity=NotifyBillingImplAug |  msgID=146 / User server connected on 2016/09/02 09:54:07\n"
        + "amdocs.ofc.process.ActivityManager::runActivity, line 516\n"
        + "/Fri Sep 02 10:12:21 ART 2016/ AL Faild to instantiate activity (error): Failed to initialize Notify Billing activity.\n"
        + "=====> \n"
        + "|E |10:12:21 |52 |server |CorrelationID=OmsServer_52_1472821941500 |StepID=YfsAAA8AyVcKpzSzj6g |OA=28330 |Activity=NotifyBillingImplAug |  msgID=146 / User server connected on 2016/09/02 09:54:07\n"
        + "amdocs.ofc.process.ActivityManager::runActivity, line 516\n"
        + "/Fri Sep 02 10:12:21 ART 2016/ AL Faild to instantiate activity (error): Failed to initialize Notify Billing activity.\n"
        + "=====",
        
        "<Error> <APM> <BEA-000000> <|E |10:12:21 |52 |server |CorrelationID=OmsServer_52_1472821941500 |StepID=YfsAAA8AyVcKpzSzj6g |OA=28330 |Activity=NotifyBillingImplAug |  msgID=1000671 / User server connected on 2016/09/02 09:54:07\n"
        + "amdocs.ofc.process.AbstractActivity::createException, line 1193\n"
        + "/Fri Sep 02 10:12:21 ART 2016/ ConnectorExceptionExtCN (error): call to UpdateSubscriber failed with error IMPLEMENTATION and error message (CM1-000369) No se ha configurado la distribuci?n de cargo recurrente predeterminada para el abonado..\n"
        + "=====> \n"
        + "|E |10:12:21 |52 |server |CorrelationID=OmsServer_52_1472821941500 |StepID=YfsAAA8AyVcKpzSzj6g |OA=28330 |Activity=NotifyBillingImplAug |  msgID=1000671 / User server connected on 2016/09/02 09:54:07\n"
        + "amdocs.ofc.process.AbstractActivity::createException, line 1193\n"
        + "/Fri Sep 02 10:12:21 ART 2016/ ConnectorExceptionExtCN (error): call to UpdateSubscriber failed with error IMPLEMENTATION and error message (CM1-000369) No se ha configurado la distribuci?n de cargo recurrente predeterminada para el abonado..\n"
        + "====="
    };

    @Test
    public void create() throws IOException {
        File file = new File("target/weblogic.20160902_012611.log"); 
        
        LogGenerator log = new LogGenerator();
        log.setLogLines(LOG_LINES);
        log.create(file, LocalDate.now(), 1000);
        
        assertTrue(file.isFile());
    }
    
    public static void main(String[] args) {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MMM d, yyyy hh:mm:ss a 'ART'");
        System.out.println(now.format(dtf));
    }

}
