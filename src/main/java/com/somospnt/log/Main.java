/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.somospnt.log;

import com.somospnt.log.samples.EsbWeblogic;
import com.somospnt.log.samples.OmsWeblogic;
import com.somospnt.log.samples.SomWeblogic;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Random;

public class Main {
    
    private static final int DAYS = 30;

    public static void main(String[] args) throws IOException {
        create("target/oms-weblogic-mock.log", OmsWeblogic.LOG_LINES, OmsWeblogic.DATE_PATTERN);
        create("target/esb-weblogic-mock.log", EsbWeblogic.LOG_LINES, EsbWeblogic.DATE_PATTERN);
        create("target/som-weblogic-mock.log", SomWeblogic.LOG_LINES, SomWeblogic.DATE_PATTERN);
        
    }
    
    private static void create(String filename, String[] logLines, String datePattern) throws IOException {
        Random random = new Random();
        File file = new File(filename); 
        LocalDate date = LocalDate.now();
        
        LogGenerator log = new LogGenerator();
        log.setLogLines(logLines);
        log.setDateTimeFormat(datePattern);
        
        for (int i = 0; i < DAYS; i++) {
            System.out.println(file.getName() + ": Generando dia " + date.toString());
            int lineCount = random.nextInt(1000) + 500;
            log.create(file, date, lineCount);            
            date = date.minusDays(1);
        }        
    }
        
}
