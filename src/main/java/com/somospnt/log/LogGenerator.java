package com.somospnt.log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class LogGenerator {
    
    private String[] logLines;
    private Random random = new Random();
    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("'<'MMM d, yyyy hh:mm:ss a 'ART> '");

    public void setLogLines(String[] logLines) {
        this.logLines = logLines;
    }
    
    public void setDateTimeFormat(String pattern) {
        dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);
    }

    public void create(File file, LocalDate date, int lineCount) throws IOException {
        try(FileWriter fw = new FileWriter(file, true)) {
            for (int i = 0; i < lineCount; i++) {            
                String line = getRandomLog();
                fw.write(getRandomDate(date));
                fw.write(line);
                fw.write("\n");
            }            
        }
    }
    
    private String getRandomLog() {
        return logLines[random.nextInt(logLines.length)];
    } 
    
    private String getRandomDate(LocalDate date) {
        int hour = random.nextInt(24);
        int minute = random.nextInt(60);
        int second = random.nextInt(60);
        LocalDateTime dateTime = date.atTime(hour, minute, second);
        return dateTime.format(dateTimeFormatter);
    }
    
}
